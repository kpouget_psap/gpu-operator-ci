Automated deployment and testing of GPU operator commits
========================================================

Steps to deploy:
----------------

Tag a node for loading the `fuse` module, and deploy the NTO profile:

```
NODE_NAME=...
oc label node/$NODE_NAME tuned.module.fuse=
oc create -f tuned_module_fuse.yaml
```

Create the `gpu-operator-ci` namespace and imagestream:


```
oc create -f operator_namespace.yaml
oc create -f operator_imagestream.yaml
```

Create the helper image, by installing and properly configuring

`podman` and `git`.

```
./build_helper_image.sh
```

This runs a custom Pod in the helper image, to clones the GPU-operator
Git repository, builds the operator image using the Ubi8 Dockerfile,
and pushes it to the image-stream.

```
./build_operator_image.sh
```

And finally, deploy the GPU-operator by cloning locally the Git
repository and deploying the helm-chart.
