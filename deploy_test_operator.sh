#! /bin/bash

set -ex

OPERATOR_NAME=gpu-operator
NAMESPACE=gpu-operator

OPERATOR_GIT_REPO=https://gitlab.com/kpouget_psap/gpu-operator.git
OPERATOR_GIT_REF=devel

CI_ARTIFACTS_GIT_REPO=https://github.com/kpouget/ci-artifacts.git
CI_ARTIFACTS_GIT_REF=commit-ci

# https://stackoverflow.com/a/21189044/341106
function parse_yaml {
   local prefix=$2
   local s='[[:space:]]*' w='[a-zA-Z0-9_]*' fs=$(echo @|tr @ '\034')
   sed -ne "s|^\($s\):|\1|" \
        -e "s|^\($s\)\($w\)$s:$s[\"']\(.*\)[\"']$s\$|\1$fs\2$fs\3|p" \
        -e "s|^\($s\)\($w\)$s:$s\(.*\)$s\$|\1$fs\2$fs\3|p"  $1 |
   awk -F$fs '{
      indent = length($1)/2;
      vname[indent] = $2;
      for (i in vname) {if (i > indent) {delete vname[i]}}
      if (length($3) > 0) {
         vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])("_")}
         printf("%s%s%s=\"%s\"\n", "'$prefix'",vn, $2, $3);
      }
   }'
}

deploy() {
    cd /tmp

    rm -rf gpu-operator
    git clone $OPERATOR_GIT_REPO -b $OPERATOR_GIT_REF --depth 1 gpu-operator

    cd gpu-operator


    SOURCE=./deployments/gpu-operator

    set +x
    declare -A HELM_values
    while read line; do
        key=$(echo $line | cut -d= -f1)
        value=$(echo $line | cut -d= -f2 | sed 's/^"//' | sed 's/"$//')
        HELM_values[$key]=$value
    done <<< $(parse_yaml deployments/gpu-operator/values.yaml)
    set -x

    #helm template --debug # <-- this is for debugging helm install
    helm install \
     $OPERATOR_NAME $SOURCE \
     --devel \
     \
     --set operator.repository=image-registry.openshift-image-registry.svc:5000/gpu-operator \
     --set operator.image=gpu-operator-ci \
     --set operator.version=gpu-operator \
     \
     --set platform.openshift=true \
     --set operator.defaultRuntime=crio \
     --set nfd.enabled=false \
     \
     --set operator.validator.version=${HELM_values[operator_validator_version]}-ubi8 \
     --set toolkit.version=${HELM_values[toolkit_version]}-ubi8 \
     --set devicePlugin.version=${HELM_values[devicePlugin_version]}-ubi8 \
     --set dcgmExporter.version=${HELM_values[dcgmExporter_version]/-ubuntu*/}-ubi8 \
     \
     --namespace $NAMESPACE \
     --wait
}

test_operator() {
    cd /tmp

    rm -rf ci-artifacts
    git clone $CI_ARTIFACTS_GIT_REPO -b $CI_ARTIFACTS_GIT_REF --depth 1 ci-artifacts

    cd ci-artifacts

    ansible-playbook \
        -e reinstall=no \
        -e install_operator_from_hub=no \
        -e aws_add_gpu_node=no \
        -e aws_check=false \
        -i /tmp/ci-artifacts/inventory/hosts \
        playbooks/nvidia-gpu-operator-ci.yml
}


undeploy() {
    echo helm uninstall --namespace $NAMESPACE $OPERATOR_NAME
}

deploy
test_operator
undeploy
