#! /bin/bash

set -x

oc create -f operator_namespace.yaml
oc create -f operator_imagestream.yaml

./build_helper_image.sh

./build_operator_image.sh

./deploy_test_operator.sh
