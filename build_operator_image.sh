#! /bin/bash

oc delete -f operator_image_builder_script.yaml
oc delete -f operator_image_builder_pod.yaml

oc delete imagestreamtag gpu-operator-ci:gpu-operator-ci -n gpu-operator-ci

oc create -f operator_image_builder_script.yaml

BUILDER_DOCKERCFG=$(oc get secrets -oname -n gpu-operator-ci | grep builder-dockercfg | cut -d/ -f2)

sed "s|BUILDER_DOCKERCFG|$BUILDER_DOCKERCFG|" operator_image_builder_pod.yaml \
    | oc create -f-

LOG_CMD="oc logs -f operator-image-builder-pod -n gpu-operator-ci"
echo $LOG_CMD
while ! $LOG_CMD; do
      sleep 2
done
