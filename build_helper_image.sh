#! /bin/bash

oc delete imagestreamtag gpu-operator-ci:builder_helper -n gpu-operator-ci
oc delete -f operator_image_helper_builder.yaml

#

oc create -f operator_image_helper_builder.yaml

LOG_CMD="oc logs -f bc/helper-image-builder -n gpu-operator-ci"
echo $LOG_CMD

$LOG_CMD
